package by.demo.app.dto;

import java.util.Date;
import java.util.List;

/**
 * Dto for creation new order.
 * createDate example: 2016-03-09T15:14:00.782Z
 * @author Kira Stepina
 * @since 2016-03-09
 */
public class OrderDTO {
    private Date createDate;
    private List<String> productsList;

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public List<String> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<String> productsList) {
        this.productsList = productsList;
    }
}

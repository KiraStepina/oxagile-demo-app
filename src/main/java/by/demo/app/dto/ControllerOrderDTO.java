package by.demo.app.dto;

import java.util.Date;
import java.util.List;

/**
 * DTO that return by OrderController functions.
 * @author Kira Stepina
 * @since 2016-03-18
 */
public class ControllerOrderDTO {
    private Long orderId;
    private Date createDate;
    private List<ControllerProductDTO> productsList;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public List<ControllerProductDTO> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<ControllerProductDTO> productsList) {
        this.productsList = productsList;
    }
}

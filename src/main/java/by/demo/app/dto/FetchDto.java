package by.demo.app.dto;

/**
 * Dto for reading list of objects. Page starts from 0.
 * @author Kira Stepina
 * @since 2016-03-04
 */
public class FetchDTO {
    private int page;
    private int pageSize;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}

package by.demo.app.service;

import by.demo.app.dto.ControllerOrderDTO;
import by.demo.app.dto.ControllerProductDTO;
import by.demo.app.dto.FetchDTO;
import by.demo.app.dto.OrderDTO;
import by.demo.app.repository.entity.OrderEntity;
import by.demo.app.repository.entity.ProductEntity;
import by.demo.app.repository.repo.OrderRepository;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Services for making business operations on Order entity.
 * @author Kira Stepina
 * @since 2016-03-05
 */
@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductService productService;

    private MapperFactory mapperFactory;

    @PostConstruct
    private void configureOrderMapping(){
        mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(OrderEntity.class, ControllerOrderDTO.class)
                .field("orderId", "orderId")
                .field("createDate", "createDate")
                .field("productEntityList{productId}", "productsList{productId}")
                .field("productEntityList{productName}", "productsList{productName}")
                .byDefault()
                .register();
    }

    @Transactional
    public ControllerOrderDTO createOrder(OrderDTO orderDTO) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCreateDate(orderDTO.getCreateDate());
        List<ProductEntity> productEntityList = new ArrayList<>();
        for (String currentProduct : orderDTO.getProductsList()) {
            ProductEntity productEntity = productService.findByProductName(currentProduct);
            Assert.notNull(productEntity);
            productEntityList.add(productEntity);
        }
        orderEntity.setProductEntityList(productEntityList);
        OrderEntity newOrderEntity = orderRepository.save(orderEntity);
        return mapperFactory.getMapperFacade().map(newOrderEntity, ControllerOrderDTO.class);
    }

    @Transactional
    public void deleteById(Long orderId) {
        orderRepository.delete(orderId);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ControllerOrderDTO> readOrderList(Integer page, Integer pageSize) {
        PageRequest pageRequest = new PageRequest(page, pageSize);
        Page<OrderEntity> orderEntityPage = orderRepository.findAll(pageRequest);
        List<OrderEntity> orderEntityList = orderEntityPage.getContent();
        List<ControllerOrderDTO> controllerOrderDTOList = new ArrayList<>();
        for (OrderEntity currentOrderEntity:orderEntityList){
            ControllerOrderDTO controllerOrderDTO = mapperFactory.getMapperFacade().map(currentOrderEntity, ControllerOrderDTO.class);
            controllerOrderDTOList.add(controllerOrderDTO);
        }
        return controllerOrderDTOList;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ControllerOrderDTO> findOrderByProduct(String productName) {
        List<OrderEntity> orderEntityList = orderRepository.findOrderByProduct(productName);
        List<ControllerOrderDTO> controllerOrderDTOList = new ArrayList<>();
        for (OrderEntity currentOrderEntity:orderEntityList){
            ControllerOrderDTO controllerOrderDTO = mapperFactory.getMapperFacade().map(currentOrderEntity, ControllerOrderDTO.class);
            controllerOrderDTOList.add(controllerOrderDTO);
        }
        return controllerOrderDTOList;
    }

    @Transactional
    public ControllerOrderDTO updateOrder(OrderEntity orderEntity) {
        OrderEntity baseOrderEntity = orderRepository.findOne(orderEntity.getOrderId());
        List<ProductEntity> baseProductEntityList = baseOrderEntity.getProductEntityList();
        for (ProductEntity currentProductEntity : orderEntity.getProductEntityList()) {
            ProductEntity repoProductEntity = productService.findByProductName(currentProductEntity.getProductName());
            Assert.notNull(repoProductEntity);
            if (!baseProductEntityList.stream().anyMatch(productEntity ->
                    productEntity.getProductId().equals(repoProductEntity.getProductId())
                            && productEntity.getProductName().equals(repoProductEntity.getProductName()))) {
                baseProductEntityList.add(repoProductEntity);
            }
        }
        OrderEntity newOrderEntity = orderRepository.save(baseOrderEntity);
        return mapperFactory.getMapperFacade().map(newOrderEntity, ControllerOrderDTO.class);
    }
}

package by.demo.app.service;

import by.demo.app.dto.ControllerUserDTO;
import by.demo.app.dto.FetchDTO;
import by.demo.app.dto.UserDTO;
import by.demo.app.repository.entity.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * @author Kira Stepina
 * @since 2016-03-13
 */
public interface UserInfoService extends UserDetailsService {
    ControllerUserDTO createUser(UserDTO userDTO);

    void deleteUserById(Long id);

    void deleteByEmail(String email);

    ControllerUserDTO findUserByEmail(String email);

    List<ControllerUserDTO> readUserList(Integer page, Integer pageSize);

    ControllerUserDTO updateUser(UserEntity userEntity);

    void updatePassword(Long userId, String newPassword);
}

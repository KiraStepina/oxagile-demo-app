package by.demo.app.service;

import by.demo.app.dto.ControllerUserDTO;
import by.demo.app.dto.FetchDTO;
import by.demo.app.dto.UserDTO;
import by.demo.app.repository.entity.SecurityGroupEntity;
import by.demo.app.repository.entity.UserEntity;
import by.demo.app.repository.repo.UserRepository;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Services for making business operations on User entity.
 *
 * @author Kira Stepina
 * @since 2016-03-02
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityGroupService securityGroupService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private MapperFactory mapperFactory;

    @PostConstruct
    private void configureUserMapping(){
        mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(UserEntity.class, ControllerUserDTO.class)
                .field("id", "userId")
                .field("firstName", "firstName")
                .field("lastName", "lastName")
                .field("email", "email")
                .field("age", "age")
                .field("securityGroupEntity.role", "role")
                .byDefault()
                .register();
    }

    @Transactional
    @Override
    public ControllerUserDTO createUser(UserDTO userDTO) {
        String encodedPass = bCryptPasswordEncoder.encode(userDTO.getPassword());
        SecurityGroupEntity securityGroupEntity = securityGroupService.findUserRole(userDTO.getRole());
        Assert.notNull(securityGroupEntity);
        Assert.isTrue(userDTO.getAge()<150);
        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName(userDTO.getFirstName());
        userEntity.setLastName(userDTO.getLastName());
        userEntity.setAge(userDTO.getAge());
        userEntity.setPassword(encodedPass);
        userEntity.setEmail(userDTO.getEmail());
        userEntity.setSecurityGroupEntity(securityGroupEntity);
        UserEntity newUserEntity = userRepository.save(userEntity);
        return mapperFactory.getMapperFacade().map(newUserEntity, ControllerUserDTO.class);
    }

    @Transactional
    @Override
    public void deleteUserById(Long id) {
        userRepository.delete(id);
    }

    @Transactional
    @Override
    public void deleteByEmail(String email) {
        UserEntity userEntity = userRepository.findByName(email);
        userRepository.delete(userEntity);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public ControllerUserDTO findUserByEmail(String email) {
        UserEntity newUserEntity = userRepository.findByName(email);
        Assert.notNull(newUserEntity);
        return mapperFactory.getMapperFacade().map(newUserEntity, ControllerUserDTO.class);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public List<ControllerUserDTO> readUserList(Integer page, Integer pageSize) {
        PageRequest pageRequest = new PageRequest(page, pageSize);
        Page<UserEntity> userEntityPage = userRepository.findAll(pageRequest);
        List<UserEntity> userEntityList = userEntityPage.getContent();
        List<ControllerUserDTO> controllerUserDTOList = new ArrayList<>();
        for (UserEntity currentUserEntity:userEntityList){
            ControllerUserDTO controllerUserDTO = mapperFactory.getMapperFacade().map(currentUserEntity, ControllerUserDTO.class);
            controllerUserDTOList.add(controllerUserDTO);
        }
        return controllerUserDTOList;
    }

    @Transactional
    @Override
    public ControllerUserDTO updateUser(UserEntity userEntity) {
        UserEntity baseUserEntity = userRepository.findByName(userEntity.getEmail());
        baseUserEntity.setFirstName(userEntity.getFirstName());
        baseUserEntity.setLastName(userEntity.getLastName());
        Assert.isTrue(userEntity.getAge()<150);
        baseUserEntity.setAge(userEntity.getAge());
        SecurityGroupEntity securityGroupEntity = securityGroupService.findUserRole(userEntity.getSecurityGroupEntity()
                .getRole());
        Assert.notNull(securityGroupEntity);
        baseUserEntity.setSecurityGroupEntity(securityGroupEntity);
        UserEntity newUserEntity = userRepository.save(baseUserEntity);
        return mapperFactory.getMapperFacade().map(newUserEntity, ControllerUserDTO.class);
    }

    @Transactional
    @Override
    public void updatePassword(Long userId, String newPassword) {
        UserEntity userEntity = userRepository.findOne(userId);
        Assert.notNull(userEntity);
        String newEncodedPassword = bCryptPasswordEncoder.encode(newPassword);
        userEntity.setPassword(newEncodedPassword);
        userRepository.save(userEntity);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public UserDetails loadUserByUsername(String username) {
        return userRepository.findByName(username);
    }
}

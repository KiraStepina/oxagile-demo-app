package by.demo.app.service;

import by.demo.app.repository.entity.SecurityGroupEntity;
import by.demo.app.repository.repo.SecurityGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Services for making business operations on SecurityGroup entity.
 * @author Kira Stepina
 * @since 2016-03-04
 */
@Service
public class SecurityGroupService {
    @Autowired
    private SecurityGroupRepository securityGroupRepository;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public SecurityGroupEntity findUserRole(String role){
        return securityGroupRepository.findSecurityGroupByName(role);
    }
}

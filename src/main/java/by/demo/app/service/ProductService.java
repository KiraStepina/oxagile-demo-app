package by.demo.app.service;

import by.demo.app.repository.entity.ProductEntity;
import by.demo.app.repository.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Services for making business operations on Product entity.
 * @author Kira Stepina
 * @since 2016-03-10
 */
@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ProductEntity findByProductName(String productName){
        return productRepository.findByProductName(productName);
    }
}

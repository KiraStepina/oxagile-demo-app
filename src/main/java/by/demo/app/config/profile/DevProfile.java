package by.demo.app.config.profile;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Kira Stepina
 * @since 2016-03-17
 */
@Configuration
@PropertySource("classpath:db-config-dev.properties")
@Profile("dev")
public class DevProfile {
}

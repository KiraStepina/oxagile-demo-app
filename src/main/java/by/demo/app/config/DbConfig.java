package by.demo.app.config;

import liquibase.integration.spring.SpringLiquibase;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Spring configuration for jdbc, hibernate and liquibase.
 * @author Kira Stepina
 * @since 2016-03-02
 */
@Configuration
@EnableJpaRepositories(basePackages = "by.demo.app.repository.repo")
@EnableTransactionManagement
public class DbConfig {
    @Value("${jdbc.driver.name:org.h2.Driver}")
    private String driverName;
    @Value("${jdbc.connection.url:jdbc:h2:mem:test}")
    private String url;
    @Value("${jdbc.connection.user: }")
    private String user;
    @Value("${jdbc.connection.password: }")
    private String password;
    @Value("${hibernate.dialect:org.hibernate.dialect.H2Dialect}")
    private String hibernateDialect;
    @Value("${hibernate.format_sql:true}")
    private String formatSql;
    @Value("${hibernate.show_sql:true}")
    private String showSql;
    @Value("${hibernate.ejb.naming_strategy:org.hibernate.cfg.ImprovedNamingStrategy}")
    private String namingStrategy;
    @Value("${config.name:default}")
    private String configName;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }


    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driverName);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        liquibase();
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setPackagesToScan("by.demo.app.repository.entity");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        entityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
        Properties jpaProterties = new Properties();
        jpaProterties.put("hibernate.format_sql", formatSql);
        jpaProterties.put("hibernate.show_sql", showSql);
        jpaProterties.put("hibernate.dialect", hibernateDialect);
        jpaProterties.put("hibernate.ejb.naming_strategy", namingStrategy);
        jpaProterties.put("hibernate.hbm2ddl.auto", "validate");
        entityManagerFactoryBean.setPersistenceUnitName("commonUnit");
        entityManagerFactoryBean.setJpaProperties(jpaProterties);
        return entityManagerFactoryBean;
    }

    @Bean
    @Primary
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerFactory().getObject());
        return manager;
    }

    @Bean
    public SpringLiquibase liquibase(){
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource());
        liquibase.setChangeLog("classpath:/db/changelog/db.changelog-master.yaml");
        return liquibase;
    }
}

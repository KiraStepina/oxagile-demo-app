package by.demo.app.repository.entity;

import javax.persistence.*;

/**
 * @author Kira Stepina
 * @since 2016-03-05
 */
@Entity
@Table(name = "products")
public class ProductEntity {
    @Id
    @GeneratedValue
    @Column(name = "product_id")
    private Long productId;
    @Column(name = "product_name")
    private String productName;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}

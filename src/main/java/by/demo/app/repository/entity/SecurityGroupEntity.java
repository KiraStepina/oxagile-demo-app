package by.demo.app.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;

/**
 * @author Kira Stepina
 * @since 2016-03-03
 */
@Entity
@Table(name = "security_group")
public class SecurityGroupEntity implements GrantedAuthority {
    @Id
    @Column(name = "group_id")
    private Long groupId;
    @Column(name = "role")
    private String role;
    @JsonIgnore
    @OneToMany(mappedBy = "securityGroupEntity")
    private List<UserEntity> userEntityList;

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<UserEntity> getUserEntityList() {
        return userEntityList;
    }

    public void setUserEntityList(List<UserEntity> userEntityList) {
        this.userEntityList = userEntityList;
    }

    @Override
    public String getAuthority() {
        return role;
    }
}

package by.demo.app.repository.repo;

import by.demo.app.repository.entity.SecurityGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Kira Stepina
 * @since 2016-03-04
 */
public interface SecurityGroupRepository extends JpaRepository<SecurityGroupEntity, Long> {
    @Query("select u from SecurityGroupEntity u where u.role = :roleName")
    SecurityGroupEntity findSecurityGroupByName(@Param("roleName") String roleName);
}

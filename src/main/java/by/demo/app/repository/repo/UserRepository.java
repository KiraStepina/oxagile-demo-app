package by.demo.app.repository.repo;

import by.demo.app.repository.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Kira Stepina
 * @since 2016-03-02
 */
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    @Query("select u from UserEntity u where u.email = :user")
    UserEntity findByName(@Param("user") String name);
}

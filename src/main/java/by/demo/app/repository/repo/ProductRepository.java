package by.demo.app.repository.repo;

import by.demo.app.repository.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Kira Stepina
 * @since 2016-03-05
 */
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    @Query("select u from ProductEntity u where u.productName = :product_name")
    ProductEntity findByProductName(@Param("product_name") String productName);
}

package by.demo.app.repository.repo;

import by.demo.app.repository.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Kira Stepina
 * @since 2016-03-05
 */
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    @Query("select o from OrderEntity o inner join o.productEntityList productEntityList where " +
            "productEntityList.productName = :product_name")
    List<OrderEntity> findOrderByProduct(@Param("product_name") String productName);
}

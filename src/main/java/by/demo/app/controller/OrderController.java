package by.demo.app.controller;

import by.demo.app.dto.ControllerOrderDTO;
import by.demo.app.dto.FetchDTO;
import by.demo.app.dto.OrderDTO;
import by.demo.app.repository.entity.OrderEntity;
import by.demo.app.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controllers for creation new order, deletion order by id, reading order list, finding order list by product name,
 * updating existing order.
 * All operations are available for users with "admin" and "user" user roles.
 * @author Kira Stepina
 * @since 2016-03-09
 */
@RestController
@PreAuthorize("hasAnyAuthority('admin', 'user')")
@RequestMapping("/orders")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ControllerOrderDTO create(@RequestBody OrderDTO orderDTO) {
        return orderService.createOrder(orderDTO);
    }

    @RequestMapping(value = "/{orderId}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable("orderId") Long orderId){
        orderService.deleteById(orderId);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<ControllerOrderDTO> read(@RequestParam("page") Integer page, @RequestParam("pageSize") Integer pageSize){
        return orderService.readOrderList(page, pageSize);
    }

    @RequestMapping(value = "/products/{productName}", method = RequestMethod.GET)
    public List<ControllerOrderDTO> findOrderByProduct(@PathVariable("productName") String productName){
        return orderService.findOrderByProduct(productName);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ControllerOrderDTO update(@RequestBody OrderEntity orderEntity){
        return orderService.updateOrder(orderEntity);
    }
}

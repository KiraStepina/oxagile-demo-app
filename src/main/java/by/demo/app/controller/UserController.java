package by.demo.app.controller;

import by.demo.app.dto.ControllerUserDTO;
import by.demo.app.dto.FetchDTO;
import by.demo.app.dto.UserDTO;
import by.demo.app.repository.entity.UserEntity;
import by.demo.app.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controllers for creation new user, deletion user by id, deletion user by email, finding user by email, reading user
 * list, updating existing user.
 * All operations are available only for users with "admin" user role.
 * @author Kira Stepina
 * @since 2016-03-04
 */
@RestController
@PreAuthorize("hasAuthority('admin')")
@RequestMapping("/admin")
public class UserController {
    @Autowired
    private UserInfoService userInfoService;

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ControllerUserDTO create(@RequestBody UserDTO userDTO){
        return userInfoService.createUser(userDTO);
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable("userId") Long userId){
        userInfoService.deleteUserById(userId);
    }

    @RequestMapping(value = "/users/{email}", method = RequestMethod.DELETE)
    public void deleteByEmail(@PathVariable("email") String email){
        userInfoService.deleteByEmail(email);
    }

    @RequestMapping(value = "/users/{email}", method = RequestMethod.GET)
    public ControllerUserDTO findUserByEmail(@PathVariable String email){
        return userInfoService.findUserByEmail(email);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<ControllerUserDTO> read(@RequestParam("page") Integer page, @RequestParam("pageSize") Integer pageSize) {
        return userInfoService.readUserList(page, pageSize);
    }

    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public ControllerUserDTO update(@RequestBody UserEntity userEntity){
        return userInfoService.updateUser(userEntity);
    }

    @RequestMapping(value = "/users/passwords/{userId}", method = RequestMethod.PUT)
    public void updatePassword(@RequestBody String newPassword, @PathVariable("userId") Long userId) {
        userInfoService.updatePassword(userId, newPassword);
    }

}

import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by stsepinaku on 03.03.2016.
 */
public class UserConversionTest {
    @Ignore
    @Test
    public void userConversion() throws IOException {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        List<List<String>> result = Lists.newArrayList();
        List<String> input = FileUtils.readLines(
                new File(getClass().getClassLoader().getResource("users.csv").getPath()));
        for (String row : input) {
            //String[] data = row.split("\t");
            result.add(Lists.newArrayList(encoder.encode(row)));
        }
        FileUtils.writeLines(new File("users_result.csv"), result);
    }
}

package by.demo.app.service;

import by.demo.app.dto.UserDTO;
import by.demo.app.repository.entity.SecurityGroupEntity;
import by.demo.app.repository.entity.UserEntity;
import by.demo.app.repository.repo.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Kira Stepina
 * @since 2016-03-10
 */
@RunWith(MockitoJUnitRunner.class)
public class UserInfoServiceTest {
    @InjectMocks
    private UserInfoServiceImpl userInfoService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private SecurityGroupService securityGroupService;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Test
    public void testCreateUser() throws Exception {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setFirstName("User First Name");
        userEntity.setLastName("User Last Name");
        userEntity.setAge(111);
        userEntity.setEmail("userEmail");
        SecurityGroupEntity securityGroupEntity = new SecurityGroupEntity();
        securityGroupEntity.setGroupId(2L);
        securityGroupEntity.setRole("user");
        userEntity.setSecurityGroupEntity(securityGroupEntity);
        Mockito.when(bCryptPasswordEncoder.encode("password")).thenReturn("EncodedPasswordTopSecret");
        Mockito.when(securityGroupService.findUserRole("user")).thenReturn(securityGroupEntity);
        ArgumentMatcher<UserEntity> argThat = new ArgumentMatcher<UserEntity>() {
            @Override
            public boolean matches(Object argument) {
                if (!(argument instanceof UserEntity)) {
                    return false;
                }
                UserEntity casted = (UserEntity) argument;
                return casted.getPassword().equals("EncodedPasswordTopSecret")
                        && casted.getSecurityGroupEntity() != null;
            }
        };
        Mockito.when(userRepository.save(Mockito.argThat(argThat))).thenReturn(userEntity);

        UserDTO userDTO = new UserDTO();
        userDTO.setPassword("password");
        userDTO.setRole("user");
        userDTO.setFirstName("User First Name");
        userDTO.setLastName("User Last Name");
        userDTO.setEmail("userEmail");
        userDTO.setAge(111);
        userInfoService.createUser(userDTO);
        Mockito.verify(userRepository).save(Mockito.argThat(argThat));
        Mockito.validateMockitoUsage();
    }

    @Test
    public void testDeleteUserById() throws Exception {
        Long userId = 0L;
        Mockito.doNothing().when(userRepository).delete(userId);
        userInfoService.deleteUserById(userId);
        Mockito.verify(userRepository).delete(userId);
    }

    @Test
    public void testDeleteByEmail() throws Exception {
        UserEntity userEntity = new UserEntity();
        Mockito.when(userRepository.findByName("email")).thenReturn(userEntity);
        Mockito.doNothing().when(userRepository).delete(userEntity);
        userInfoService.deleteByEmail("email");
        Mockito.verify(userRepository).delete(userEntity);
    }

    @Test
    public void testFindUserByEmail() throws Exception {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setFirstName("User First Name");
        userEntity.setLastName("User Last Name");
        userEntity.setAge(111);
        userEntity.setEmail("userEmail");
        SecurityGroupEntity securityGroupEntity = new SecurityGroupEntity();
        securityGroupEntity.setGroupId(2L);
        securityGroupEntity.setRole("user");
        userEntity.setSecurityGroupEntity(securityGroupEntity);
        Mockito.when(userRepository.findByName("email")).thenReturn(userEntity);
        userInfoService.findUserByEmail("email");
        Mockito.verify(userRepository).findByName("email");
    }

    @Test
    public void testUpdateUser() throws Exception {
        UserEntity userBaseEntity = new UserEntity();
        userBaseEntity.setPassword("p");
        userBaseEntity.setId(1l);
        UserEntity userEntity = new UserEntity();
        SecurityGroupEntity securityGroupEntity = new SecurityGroupEntity();
        Mockito.when(userRepository.findByName("userEmail")).thenReturn(userBaseEntity);
        Mockito.when(securityGroupService.findUserRole("testRole")).thenReturn(securityGroupEntity);
        ArgumentMatcher<UserEntity> argThat = new ArgumentMatcher<UserEntity>() {
            @Override
            public boolean matches(Object argument) {
                if (!(argument instanceof UserEntity)) {
                    return false;
                }
                UserEntity casted = (UserEntity) argument;
                return casted.getFirstName().equals(userEntity.getFirstName())
                        && casted.getLastName().equals(userEntity.getLastName())
                        && casted.getAge().equals(userEntity.getAge())
                        && casted.getSecurityGroupEntity().getRole().equals(userEntity.getSecurityGroupEntity().getRole())
                        && casted.getPassword().equals(userBaseEntity.getPassword())
                        && casted.getId().equals(userBaseEntity.getId());
            }
        };
        Mockito.when(userRepository.save(Mockito.argThat(argThat))).thenReturn(userBaseEntity);

        securityGroupEntity.setRole("testRole");
        userEntity.setSecurityGroupEntity(securityGroupEntity);
        userEntity.setPassword("password");
        userEntity.setFirstName("User First Name");
        userEntity.setLastName("User Last Name");
        userEntity.setEmail("userEmail");
        userEntity.setAge(111);
        userInfoService.updateUser(userEntity);
        Mockito.verify(userRepository).save(Mockito.argThat(argThat));
    }
}
package by.demo.app.service;

import by.demo.app.dto.ControllerOrderDTO;
import by.demo.app.dto.OrderDTO;
import by.demo.app.repository.entity.OrderEntity;
import by.demo.app.repository.entity.ProductEntity;
import by.demo.app.repository.repo.OrderRepository;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Kira Stepina
 * @since 2016-03-11
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {
    @InjectMocks
    private OrderService orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ProductService productService;

    @Test
    public void testCreateOrder(){
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderId(1L);
        orderEntity.setCreateDate(new Date());
        List<ProductEntity> productEntityList = new ArrayList<>();
        ProductEntity productEntity = new ProductEntity();
        productEntity.setProductId(1L);
        productEntity.setProductName("product1");
        productEntityList.add(productEntity);
        orderEntity.setProductEntityList(productEntityList);
        Mockito.when(productService.findByProductName("product1")).thenReturn(productEntity);
        ArgumentMatcher<OrderEntity> argThat = new ArgumentMatcher<OrderEntity>() {
            @Override
            public boolean matches(Object argument) {
                if (!(argument instanceof OrderEntity)) {
                    return false;
                }
                OrderEntity casted = (OrderEntity)argument;
                return casted.getProductEntityList()!=null;
            }
        };
        Mockito.when(orderRepository.save(Mockito.argThat(argThat))).thenReturn(orderEntity);

        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setCreateDate(new Date());
        List<String> productList = Lists.newArrayList("product1");
        orderDTO.setProductsList(productList);
        orderService.createOrder(orderDTO);
        Mockito.verify(orderRepository).save(Mockito.argThat(argThat));
    }

    @Test
    public void testDeleteById(){
        Long orderId = 0L;
        Mockito.doNothing().when(orderRepository).delete(orderId);
        orderService.deleteById(orderId);
        Mockito.verify(orderRepository).delete(orderId);
    }

    @Test
    public void testFindOrderByProduct(){
        List<OrderEntity> orderEntityList = Lists.newArrayList();
        Mockito.when(orderRepository.findOrderByProduct("productName")).thenReturn(orderEntityList);
        orderService.findOrderByProduct("productName");
        Mockito.verify(orderRepository).findOrderByProduct("productName");
    }

    @Test
    public void testUpdateOrder(){
        OrderEntity orderEntity = new OrderEntity();
        OrderEntity baseOrderEntity = new OrderEntity();
        ProductEntity baseProductEntity = new ProductEntity();
        baseProductEntity.setProductId(1L);
        baseProductEntity.setProductName("BaseProductName");
        List<ProductEntity> baseProductEntityList = Lists.newArrayList();
        baseProductEntityList.add(baseProductEntity);
        baseOrderEntity.setProductEntityList(Lists.newArrayList());

        Mockito.when(productService.findByProductName("ProductName")).thenReturn(baseProductEntity);
        Mockito.when(orderRepository.findOne(orderEntity.getOrderId())).thenReturn(baseOrderEntity);
        ArgumentMatcher<OrderEntity> argThat = new ArgumentMatcher<OrderEntity>() {
            @Override
            public boolean matches(Object argument) {
                if (!(argument instanceof OrderEntity)) {
                    return false;
                }
                OrderEntity casted = (OrderEntity)argument;
                return casted.getProductEntityList()!=null && casted.getProductEntityList().stream().anyMatch(productEntity ->
                        productEntity.getProductId().equals(baseProductEntity.getProductId())
                                && productEntity.getProductName().equals(baseProductEntity.getProductName()));
            }
        };
        Mockito.when(orderRepository.save(Mockito.argThat(argThat))).thenReturn(baseOrderEntity);

        orderEntity.setCreateDate(new Date());
        ProductEntity productEntity = new ProductEntity();
        productEntity.setProductName("ProductName");
        List<ProductEntity> productEntityList = Lists.newArrayList();
        productEntityList.add(productEntity);
        orderEntity.setProductEntityList(productEntityList);
        orderService.updateOrder(orderEntity);
        Mockito.verify(orderRepository).save(Mockito.argThat(argThat));
    }
}

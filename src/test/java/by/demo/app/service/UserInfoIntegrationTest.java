package by.demo.app.service;

import by.demo.app.Application;
import by.demo.app.dto.ControllerUserDTO;
import by.demo.app.dto.FetchDTO;
import by.demo.app.dto.UserDTO;
import by.demo.app.repository.entity.UserEntity;
import by.demo.app.repository.repo.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @author Kira Stepina
 * @since 2016-03-10
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest
public class UserInfoIntegrationTest {
    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testUserCreation(){
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("User First Name1");
        userDTO.setLastName("User Last Name1");
        userDTO.setPassword("user_password1");
        userDTO.setAge(111);
        userDTO.setEmail("userEmail1");
        userDTO.setRole("user");
        ControllerUserDTO controllerUserDTO = userInfoService.createUser(userDTO);
        Assert.assertNotNull(controllerUserDTO);
        Assert.assertEquals(userDTO.getFirstName(), controllerUserDTO.getFirstName());
        Assert.assertEquals(userDTO.getLastName(), controllerUserDTO.getLastName());
        Assert.assertEquals(userDTO.getAge(), controllerUserDTO.getAge());
        Assert.assertEquals(userDTO.getEmail(), controllerUserDTO.getEmail());
        Assert.assertEquals(userDTO.getRole(), controllerUserDTO.getRole());
    }

    @Test
    public void testDeleteUserById(){
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("User First Name2");
        userDTO.setLastName("User Last Name2");
        userDTO.setPassword("user_password2");
        userDTO.setAge(111);
        userDTO.setEmail("userEmail2");
        userDTO.setRole("user");
        ControllerUserDTO controllerUserDTO =  userInfoService.createUser(userDTO);
        userInfoService.deleteUserById(controllerUserDTO.getUserId());
        Assert.assertNull(userRepository.findByName("userEmail2"));
    }

    @Test
    public void testDeleteByEmail(){
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("User First Name3");
        userDTO.setLastName("User Last Name3");
        userDTO.setPassword("user_password3");
        userDTO.setAge(111);
        userDTO.setEmail("userEmail3");
        userDTO.setRole("user");
        userInfoService.createUser(userDTO);
        userInfoService.deleteByEmail("userEmail3");
        Assert.assertNull(userRepository.findByName("userEmail3"));
    }

    @Test
    public void testFindUserByEmail(){
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("User First Name4");
        userDTO.setLastName("User Last Name4");
        userDTO.setPassword("user_password4");
        userDTO.setAge(111);
        userDTO.setEmail("userEmail4");
        userDTO.setRole("user");
        userInfoService.createUser(userDTO);
        Assert.assertNotNull(userInfoService.findUserByEmail("userEmail4"));
    }

    @Test
    public void testReadUserList(){
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("User First Name5");
        userDTO.setLastName("User Last Name5");
        userDTO.setPassword("user_password5");
        userDTO.setAge(111);
        userDTO.setEmail("userEmail5");
        userDTO.setRole("user");
        userInfoService.createUser(userDTO);
        List<ControllerUserDTO> controllerUserDTOList = userInfoService.readUserList(0, 10);
        Assert.assertTrue(controllerUserDTOList.stream().anyMatch(user->user.getEmail().equals("userEmail5")));
    }

    @Test
    public void testUpdateUser(){
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("User First Name6");
        userDTO.setLastName("User Last Name6");
        userDTO.setPassword("user_password6");
        userDTO.setAge(111);
        userDTO.setEmail("userEmail6");
        userDTO.setRole("user");
        userInfoService.createUser(userDTO);
        UserEntity userEntity = userRepository.findByName("userEmail6");
        userEntity.setFirstName("User First Name62");
        userEntity.getSecurityGroupEntity().setRole("admin");
        userInfoService.updateUser(userEntity);
        UserEntity updatedUserEntity = userRepository.findByName("userEmail6");
        Assert.assertEquals(updatedUserEntity.getFirstName(), "User First Name62");
        Assert.assertEquals(updatedUserEntity.getSecurityGroupEntity().getRole(), "admin");
    }
}

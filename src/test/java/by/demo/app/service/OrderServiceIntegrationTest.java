package by.demo.app.service;

import by.demo.app.Application;
import by.demo.app.dto.ControllerOrderDTO;
import by.demo.app.dto.FetchDTO;
import by.demo.app.dto.OrderDTO;
import by.demo.app.repository.entity.OrderEntity;
import by.demo.app.repository.entity.ProductEntity;
import by.demo.app.repository.repo.OrderRepository;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Kira Stepina
 * @since 2016-03-11
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest
public class OrderServiceIntegrationTest {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductService productService;

    @Test
    public void testCreateOrder() throws ParseException {
        OrderDTO orderDTO = new OrderDTO();
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2016-03-11");
        orderDTO.setCreateDate(date);
        List<String> productList = Lists.newArrayList("product1");
        orderDTO.setProductsList(productList);
        ControllerOrderDTO controllerOrderDTO = orderService.createOrder(orderDTO);
        Assert.assertNotNull(controllerOrderDTO);
        Assert.assertEquals(controllerOrderDTO.getCreateDate(), date);
        Assert.assertTrue(controllerOrderDTO.getProductsList().stream()
                .anyMatch(productEntity -> productEntity.getProductName().equals("product1")));
    }

    @Test
    public void testDeleteById() throws ParseException {
        OrderDTO orderDTO = new OrderDTO();
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2016-03-12");
        orderDTO.setCreateDate(date);
        List<String> productList = Lists.newArrayList("product2");
        orderDTO.setProductsList(productList);
        ControllerOrderDTO controllerOrderDTO = orderService.createOrder(orderDTO);
        orderService.deleteById(controllerOrderDTO.getOrderId());
        Assert.assertNull(orderRepository.findOne(controllerOrderDTO.getOrderId()));
    }

    @Test
    public void testReadOrderList() throws ParseException {
        OrderDTO orderDTO = new OrderDTO();
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2016-03-13");
        orderDTO.setCreateDate(date);
        List<String> productList = Lists.newArrayList("product3");
        orderDTO.setProductsList(productList);
        ControllerOrderDTO controllerOrderDTO = orderService.createOrder(orderDTO);
        List<ControllerOrderDTO> controllerOrderDTOList = orderService.readOrderList(0, 10);
        Assert.assertTrue(controllerOrderDTOList.stream().anyMatch(order -> order.getOrderId().equals(controllerOrderDTO.getOrderId())));
    }

    @Test
    public void testFindOrderByProduct() throws ParseException {
        OrderDTO orderDTO = new OrderDTO();
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2016-03-14");
        orderDTO.setCreateDate(date);
        List<String> productList = Lists.newArrayList("product1");
        orderDTO.setProductsList(productList);
        orderService.createOrder(orderDTO);
        orderDTO = new OrderDTO();
        date = new SimpleDateFormat("yyyy-MM-dd").parse("2016-03-15");
        orderDTO.setCreateDate(date);
        productList = Lists.newArrayList("product4");
        orderDTO.setProductsList(productList);
        orderService.createOrder(orderDTO);
        List<ControllerOrderDTO> controllerOrderDTOList = orderService.findOrderByProduct("product1");
        Assert.assertTrue(controllerOrderDTOList.stream().allMatch(order -> order.getProductsList().stream()
                .anyMatch(product -> product.getProductName().equals("product1"))));
    }

    @Test
    public void testUpdateOrder() throws ParseException {
        OrderDTO orderDTO = new OrderDTO();
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2016-03-16");
        orderDTO.setCreateDate(date);
        List<String> productList = Lists.newArrayList("product2");
        orderDTO.setProductsList(productList);
        ControllerOrderDTO controllerOrderDTO = orderService.createOrder(orderDTO);
        OrderEntity orderEntity = orderRepository.findOne(controllerOrderDTO.getOrderId());
        orderEntity.setProductEntityList(orderEntity.getProductEntityList().stream()
                .filter(product -> !product.getProductName().equals("product2")).collect(Collectors.toList()));
        ProductEntity productEntity = new ProductEntity();
        productEntity.setProductId(3L);
        productEntity.setProductName("product3");
        orderEntity.getProductEntityList().add(productEntity);
        orderRepository.save(orderEntity);
        Assert.assertTrue(orderRepository.findOne(orderEntity.getOrderId()).getProductEntityList().stream()
                .allMatch(product -> product.getProductName().equals("product3")));
    }
}
